from __future__ import unicode_literals
import django
import os
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
import django.db
from django.db.models.signals import pre_save
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import slugify

from .utils import get_read_time, unique_slug_generator

class PostManager(django.db.models.Manager):
    def active(self, *args, **kwargs):
        qs = self.get_queryset().filter(
                            draft=False,
                            publish__lte=timezone.now()
                            )
        return qs


def upload_location(instance, filename):
    """

    Parameters
    ----------
    filename : object
    """
    PostModel = instance.__class__
    new_id = PostModel.objects.order_by("id").last().id + 1
    return "%s/%s" %(new_id, filename)

class Post(django.db.models.Model):
    user            = django.db.models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    title           = django.db.models.CharField(max_length=120)
    slug            = django.db.models.SlugField(unique=True, blank=True)
    image           = django.db.models.ImageField(upload_to=upload_location,
                                                  null=True,
                                                  blank=True,
                                                  width_field="width_field",
                                                  height_field="height_field")
    likes           = django.db.models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='post_likes')
    height_field    = django.db.models.IntegerField(default=0)
    width_field     = django.db.models.IntegerField(default=0)
    content         = django.db.models.TextField()
    draft           = django.db.models.BooleanField(default=False)
    publish         = django.db.models.DateField(auto_now=False, auto_now_add=False)
    read_time       = django.db.models.IntegerField(default=0)
    updated         = django.db.models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp       = django.db.models.DateTimeField(auto_now=False, auto_now_add=True)

    objects         = PostManager()

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("posts:detail", kwargs={"slug": self.slug})

    # def get_api_url(self):
    #     return reverse("posts-api:detail", kwargs={"slug": self.slug})

    def get_like_url(self):
        return reverse("posts:like-toggle", kwargs={"slug": self.slug})

    def get_api_like_url(self):
        return reverse("posts:like-api-toggle", kwargs={"slug": self.slug})
    
    class Meta:
        ordering = ["-timestamp", "-updated"]

    @property
    def get_content_type(self):
        instance = self
        content_type = ContentType.objects.get_for_model(instance.__class__)
        return content_type



def pre_save_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

    if instance.content:
        read_time_var = get_read_time(instance.content)
        instance.read_time = read_time_var


pre_save.connect(pre_save_post_receiver, sender=Post)
